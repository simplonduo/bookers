<?php






// connect à la db
$host = '127.0.0.1';
$db   = 'bookers';
$user = 'kawtar';
$pass = '123'; 

$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);


function readAll(){
    global $pdo;
    $req = $pdo->query("select * from books;");
    return $req->fetchAll();
}


function create($title, $description, $image, $author,$links){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO books (title, description, image, author,links) VALUES (?,?, ?, ?, ?);');
    $req->execute([$title, $description, $image, $author,$links]);
    }

  function delete($id){
    global $pdo;

    $req = $pdo->prepare("delete from books where id=?;");
    $req->execute([$id]);
}
