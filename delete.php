<?php
include "pdo.php";

if (isset($_GET['id'])) {
    $bookIdToDelete = $_GET['id'];
    delete($bookIdToDelete);
    
    // Après la suppression, redirigez l'utilisateur vers la page list.php
    header("Location: list.php");
    exit();
}
?>


