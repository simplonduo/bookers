-- instead of creating database in your terminal one by one you write your table here and in the terminal 
-- you only write sudo mysql<init.sql as a short cut 
drop database if exists bookers;

create database bookers;
use bookers;
create table books(
    id int not null auto_increment primary key,
    title varchar(255),
    description varchar(1000),
    image varchar(1000),
    links varchar(1000),
    author varchar(100)

);

  DROP USER  if exists 'kawtar'@'localhost';
CREATE USER 'kawtar'@'localhost' IDENTIFIED BY '123';
GRANT ALL PRIVILEGES ON bookers.* TO 'kawtar'@'localhost';